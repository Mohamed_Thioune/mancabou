@extends('layouts.head')

<body>
    <div class="content-head">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="img/logo.png" alt="" class="logoNaav">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto align-items-center">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('login') }}">Se connecter</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link btn-link" href="{{ route('register') }}">Devenir membre</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
           <div class="content-title">
               <h1>Experte en <br> Leadership Transformationnel Afro Holistique</h1>
               <p class="sub-title">Spécialisée en <br> Déprogrammation <br> des Mémoires Émotionnelles Transgénérationnelles</p>
           </div>
        </div>
    </div>
    <div class="container">
        <div class="video">
            <img src="img/dameVideo.png" data-video="img/v1.mp4" title="Play Video" class="video__placeholder" />
            <button class="video__button">
                <img src="img/playImg.png" alt="">
            </button>
        </div>
        <div class="block-Apropos">
            <div class="imgBlockPropos">
                <img src="img/imgAPropos.png" alt="">
                <p class="text-exp"><strong>+25</strong><br>ANS EXP.</p>
            </div>
            <div class="secondBlock">
                <h2 class="title-APropos">A propos de nous</h2>
                <p class="description">L'afro coaching est une approche de coaching qui vise à aider les personnes d'origine africaine à se connecter à leur essence
                    et à leur potentiel. C'est un processus qui permet de se libérer des schémas de pensée limitants et de trouver la force intérieure pour atteindre ses objectifs.
                    L'afro coaching est un moyen de célébrer la richesse de la culture africaine tout en encourageant la croissance personnelle et professionnelle. En somme, c'est un voyage
                    vers l'auto-découverte et l'émancipation.</p>
                <a href="{{ route('register') }}" class="btn btnMember">Devenir membre</a>
            </div>
        </div>
        <div class="block-Nous-disposons">
            <h2>Nous disposons</h2>
            <p class="descriptionN">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            <div class="nous-disposons-group">
                <div class="nous-disposons-element">
                    <img class="imgnous-disposons-element" src="img/POWERCLASS.png" alt="">
                    <p class="title-nous-disposons-element">+40 POWERCLASS</p>
                    <p class="descriptionnous-disposons-element">Formations coaching contenant vidéos + cahiers d'exercices.</p>
                </div>
                <div class="nous-disposons-element">
                </div>
                <div class="nous-disposons-element">
                    <img class="imgnous-disposons-element" src="img/RESSOURCES.png" alt="">
                    <p class="title-nous-disposons-element">+50 RESSOURCES</p>
                    <p class="descriptionnous-disposons-element">Bibliothèque de Wallpapers, Calendrier, Workbooks, Ebooks...</p>
                </div>
    
                <div class="nous-disposons-element">
                    <img class="imgnous-disposons-element" src="img/COMMUNAUTE.png" alt="">
                    <p class="title-nous-disposons-element">+COMMUNAUTE</p>
                    <p class="descriptionnous-disposons-element">Groupe Facebook privé réservé aux membres.</p>
                </div>
                <div class="nous-disposons-element">
                    <img class="imgnous-disposons-element" src="img/DIRECT-LIVE.png" alt="">
                    <p class="title-nous-disposons-element">+DIRECT LIVE</p>
                    <p class="descriptionnous-disposons-element">Ateliers en ligne en direct sur le Powerclass du mois.</p>
                </div>
                <div class="nous-disposons-element">
                    <img class="imgnous-disposons-element" src="img/REPLAY-EVENEMENT.png" alt="">
                    <p class="title-nous-disposons-element">+REPLAY EVENEMENT</p>
                    <p class="descriptionnous-disposons-element">Exclusivité Replay sur tous nos événements filmés.</p>
                </div>
            </div>
        </div>
        <div class="block-pourquoi-nous-choisir">
            <h2>Pourquoi nous choisir</h2>
            <p class="sub-title">Nous avons</p>
            <div class="group-pourquoi-nous-choisir">
                <div class="first-block-text">
                    <div class="element-pourquoi-nous-choisir">
                        <div class="user-icone">
                            <i class="fas fa-user-alt"></i>
                        </div>
                       <div class="group-text">
                           <p class="title">Nous avons des équipes professionnels</p>
                           <p class="description">Intégrer un Coach Afro Holistique au sein de votre entreprise peut apporter plusieurs avantages, notamment :
                               Favoriser la diversité et l'inclusion en offrant des services spécifiques à la communauté afro-descendante
                               Améliorer le bien-être et la santé mentale des employés en offrant des outils de développement personnel adaptés
                               aux besoins culturels de cette communauté Favoriser la communication interculturelle et la compréhension mutuelle au sein
                               de l'entreprise Contribuer à la réduction du stress</p>
                       </div>
                    </div>
                    <div class="element-pourquoi-nous-choisir">
                        <div class="user-icone">
                            <i class="fas fa-user-alt"></i>
                        </div>
                       <div class="group-text">
                           <p class="title">Les bénéfices qu'obtient un leader à investir sur le développement spirituel</p>
                           <p class="description">Investir dans le développement spirituel peut apporter de nombreux bénéfices à un leader. 
                            Tout d'abord, cela peut aider à renforcer sa résilience face aux défis et aux pressions du quotidien. 
                            En outre, cela peut aider à améliorer sa capacité à prendre des décisions éclairées et à inspirer son équipe. 
                            Le développement spirituel peut également aider à promouvoir la compassion et la bienveillance,
                            ce qui peut améliorer les relations interpersonnelles et la collaboration. 
                            Enfin, cela peut aider à donner un sens plus profond à la vie et à la carrière d'un leader, 
                            lui permettant de se sentir plus épanoui et satisfait.</p>
                       </div>
                    </div>
                    <div class="element-pourquoi-nous-choisir">
                        <div class="user-icone">
                            <i class="fas fa-user-alt"></i>
                        </div>
                       <div class="group-text">
                           <p class="title">Nous avons comme atouts</p>
                           <p class="description">Nous choisir vous permettra d'obtenir un mindset percutant ,
                            grâce à notre équipe de professionnels expérimentés.
                            Nous nous engageons à fournir un audit de choix qui répondra à vos besoins et attentes. 
                            De plus, nous offrons une gamme de contenus pédagogiques compétitifs pour vous amener à votre réussite.</p>
                       </div>
                    </div>
                </div>
                <div class="second-block-img">
                    <div class="group-img">
                        <img src="img/pourquoi-img.png" alt="">
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <p class="attendez-vous-text">Qu'attendez vous ?</p>
                <a href="{{ route('register') }}" class="btn btnMember">Devenir membre</a>
            </div>
        </div>
    </div>
    <div class="block-nos-videos">
        <div class="container">
            <h2>Nos vidéos</h2>
            <p class="description">Accède à + de 40 Powerclass créées par des Expertes dans leur domaine.</p>
            <div id="first-block-video" class="video-text-block">
                <div class="video">
                    <img src="img/fille-noir.png" data-video="img/v1.mp4" title="Play Video" class="video__placeholder2" />
                    <button class="video__button2">
                        <img src="img/playImg.png" alt="">
                    </button>
                    <img src="img/trait-img.png" class="trait-element" alt="">
                </div>
                <div class="text-block">
                    <p class="sub-title-text">Trouver son business Model</p>
                    <p class="description-sub-text">Par Abel Stéphanie <br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor consectetur adipiscing elit consectetur adipiscing elit dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor consectetur</p>
                    <p class="price">120 000 €</p>
                </div>
            </div>
            <div id="second-block-video" class="video-text-block">
                <div class="text-block">
                    <p class="sub-title-text">Développer des Partenariats de qualité</p>
                    <p class="description-sub-text">Par Abel Stéphanie <br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor consectetur adipiscing elit consectetur adipiscing elit dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor consectetur</p>
                    <p class="price">120 000 €</p>
                </div>
                <div class="video">
                    <img src="img/fille-noir-2.png" data-video="img/v1.mp4" title="Play Video" class="video__placeholder3" />
                    <button class="video__button3">
                        <img src="img/playImg.png" alt="">
                    </button>
                    <img src="img/trait-img-2.png" class="trait-element trait2" alt="">
                </div>
            </div>
            <div class="block-power-class">
                <p class="title-power-class">D'autres powerclass</p>
                <div class="group-power-class">
                    <div class="element-group-power-class">
                        <div class="element-img">
                            <img src="img/Abel.png" alt="">
                        </div>
                        <p class="name-block">GOODBYE RELATIONS TOXIQUES !</p>
                        <p class="by-text">Par : Abel Stéphanie</p>
                        <p class="price">120 000 €</p>
                    </div>
                    <div class="element-group-power-class">
                        <div class="element-img">
                            <img src="img/Abel2.png"  alt="">
                        </div>
                        <p class="name-block business-text">POSITIONNER SON BUSINESS</p>
                        <p class="by-text">Par : Abel Stéphanie</p>
                        <p class="price">120 000 €</p>
                    </div>
                    <div class="element-group-power-class">
                        <div class="element-img">
                            <img src="img/Abel3.png" alt="">
                        </div>
                        <p class="name-block">DEVELOPPER DES PARTENARIATS DE QUALITE</p>
                        <p class="by-text">Par : Abel Stéphanie</p>
                        <p class="price">120 000 €</p>
                    </div>
                    <div class="element-group-power-class">
                        <div class="element-img">
                            <img src="img/Abel4.png" alt="">
                        </div>
                        <p class="name-block">TROUVER SON BUSINESS MODEL</p>
                        <p class="by-text">Par : Abel Stéphanie</p>
                        <p class="price">120 000 €</p>
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="{{ route('register') }}" class="btn btnMember">Devenir membre</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="block-Nos-membres">
            <h2>Nos membres</h2>
            <p class="description">Plus de 2500 femmes entrepreneures et en devenir motivées à accomplir leurs objectifs,
                partager leurs compétences entre elles et développer leur business. Voici ce qu’elles ont gagné à s’inscrire à notre Academy…</p>
    
            <div class="container">
                <div class="owl-carousel owl-theme position-relative">
                    <div class="item">
                        <div class='card-members vid-fit-reveal' id="js-fitvideo">
                            <div class="head-card">
                                <img class="placeholderImg" src="img/f2.png" alt="">
                                <button class="btn-play c-header-in">
                                    <img src="img/playImg.png" alt="">
                                </button>
                                <iframe src="img/v1.mp4" allowfullscreen id="vid-reveal" class="c-video-reveal" frameborder="0"></iframe>
                            </div>
                            <p class="sub-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class='card-members vid-fit-reveal' id="js-fitvideo">
                            <div class="head-card">
                                <img class="placeholderImg" src="img/f2.png" alt="">
                                <button class="btn-play c-header-in">
                                    <img src="img/playImg.png" alt="">
                                </button>
                                <iframe src="img/v1.mp4" allowfullscreen id="vid-reveal" class="c-video-reveal" frameborder="0"></iframe>
                            </div>
                            <p class="sub-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class='card-members vid-fit-reveal' id="js-fitvideo">
                            <div class="head-card">
                                <img class="placeholderImg" src="img/f2.png" alt="">
                                <button class="btn-play c-header-in">
                                    <img src="img/playImg.png" alt="">
                                </button>
                                <iframe src="img/v1.mp4" allowfullscreen id="vid-reveal" class="c-video-reveal" frameborder="0"></iframe>
                            </div>
                            <p class="sub-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class='card-members vid-fit-reveal' id="js-fitvideo">
                            <div class="head-card">
                                <img class="placeholderImg" src="img/f2.png" alt="">
                                <button class="btn-play c-header-in">
                                    <img src="img/playImg.png" alt="">
                                </button>
                                <iframe src="img/v1.mp4" allowfullscreen id="vid-reveal" class="c-video-reveal" frameborder="0"></iframe>
                            </div>
                            <p class="sub-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea</p>
                        </div>
                    </div>
                </div>
            </div>
    
          <!--  <div class="carousel slide multi-item-carousel" id="theCarousel">
                <div class="carousel-inner row w-100">
                    <div class="carousel-item active">
                        <div class="row w-100">
                            <div class="col-md-6 col-sm-12">
                                <div class='card-members vid-fit-reveal' id="js-fitvideo">
                                    <div class="head-card">
                                        <img class="placeholderImg" src="img/f2.png" alt="">
                                        <button class="btn-play c-header-in">
                                            <img src="img/playImg.png" alt="">
                                        </button>
                                        <iframe src="img/v1.mp4" allowfullscreen id="vid-reveal" class="c-video-reveal" frameborder="0"></iframe>
                                    </div>
                                    <p class="sub-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea</p>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class='card-members vid-fit-reveal' id="js-fitvideo">
                                    <div class="head-card">
                                        <img class="placeholderImg" src="img/f2.png" alt="">
                                        <button class="btn-play c-header-in">
                                            <img src="img/playImg.png" alt="">
                                        </button>
                                        <iframe src="img/v1.mp4" allowfullscreen id="vid-reveal" class="c-video-reveal" frameborder="0"></iframe>
                                    </div>
                                    <p class="sub-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea</p>
                                </div>
                            </div>
                        </div>
    
                    </div>
                    <div class="carousel-item">
                        <div class="row w-100">
                            <div class="col-md-6 col-sm-12">
                                <div class='card-members vid-fit-reveal' id="js-fitvideo">
                                    <div class="head-card">
                                        <img class="placeholderImg" src="img/f2.png" alt="">
                                        <button class="btn-play c-header-in">
                                            <img src="img/playImg.png" alt="">
                                        </button>
                                        <iframe src="img/v1.mp4" allowfullscreen id="vid-reveal" class="c-video-reveal" frameborder="0"></iframe>
                                    </div>
                                    <p class="sub-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea</p>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class='card-members vid-fit-reveal' id="js-fitvideo">
                                    <div class="head-card">
                                        <img class="placeholderImg" src="img/f2.png" alt="">
                                        <button class="btn-play c-header-in">
                                            <img src="img/playImg.png" alt="">
                                        </button>
                                        <iframe src="img/v1.mp4" allowfullscreen id="vid-reveal" class="c-video-reveal" frameborder="0"></iframe>
                                    </div>
                                    <p class="sub-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea</p>
                                </div>
                            </div>
                        </div>
    
                    </div>
                </div>
                <a class="carousel-control-prev" href="#theCarousel" role="button" data-slide="prev">
                    <img src="img/left.png" alt="">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#theCarousel" role="button" data-slide="next">
                    <img src="img/right.png" alt="">
                    <span class="sr-only">Next</span>
                </a>
            </div>-->
    
        </div>
    </div>
    <div class="container">
        <div class="block-voyage">
            <h2>Voyage Transformationnel Afro Holistique</h2>
            <div class="group-pays">
                <div class="element-pays">
                    <p class="pays">Sénégal</p>
                    <p class="description-voyage-pays">Nos voyages Transformationnels Afro Holistique au Sénégal</p>
                </div>
                <div class="element-pays">
                    <p class="pays">Martinique</p>
                    <p class="description-voyage-pays">Nos voyages Transformationnels Afro Holistique au Antilles</p>
                </div>
                <div class="element-pays">
                    <p class="pays">Côte d'Ivoire</p>
                    <p class="description-voyage-pays">Nos voyages Transformationnels Afro Holistique au Côte d'Ivoire</p>
                </div>
                <div class="element-pays">
                    <p class="pays">Bénin</p>
                    <p class="description-voyage-pays">Nos voyages Transformationnels Afro Holistique au Bénin</p>
                </div>
                <div class="element-pays">
                    <p class="pays">Zanzibar</p>
                    <p class="description-voyage-pays">Nos voyages Transformationnels Afro Holistique au Zanzibar</p>
                </div>
                <div class="element-pays">
                    <p class="pays">Cameroun</p>
                    <p class="description-voyage-pays">Nos voyages Transformationnels Afro Holistique au Cameroun</p>
                </div>
            </div>
        </div>
    </div>
    <div class="block-confiance">
        <div class="container">
            <h2>Ils nous ont fait confiance</h2>
            <p class="sub-description">Leurs témoignages</p>
    
           <!-- <div class="carousel slide" data-ride="carousel">
    
                    &lt;!&ndash; Indicators &ndash;&gt;
                    <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                        <li data-target="#demo" data-slide-to="2"></li>
                    </ul>
    
                    &lt;!&ndash; The slideshow &ndash;&gt;
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="img/phone.png" alt="">
                                </div>
                                <div class="col-md-3">
                                    <img src="img/phone.png" alt="">
                                </div>
                                <div class="col-md-3">
                                    <img src="img/phone.png" alt="">
                                </div>
                                <div class="col-md-3">
                                    <img src="img/phone.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="img/phone.png" alt="">
                                </div>
                                <div class="col-md-3">
                                    <img src="img/phone.png" alt="">
                                </div>
                                <div class="col-md-3">
                                    <img src="img/phone.png" alt="">
                                </div>
                                <div class="col-md-3">
                                    <img src="img/phone.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    &lt;!&ndash; Left and right controls &ndash;&gt;
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                        <img src="img/left.png" alt="">
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                        <img src="img/right.png" alt="">
                    </a>
                </div>-->
    
            <div class="gtco-testimonials">
                <div class="owl-carousel owl-carousel1 owl-theme">
                    <div>
                        <div class="card card-video-Test text-center">
                            <iframe src="img/testimonial-1.mp4" frameborder="0"></iframe>
                        </div>
                    </div>
                    <div>
                        <div class="card  text-center">
                            <div class="card-img-top">
                                <img  src="img/missy.jpg" alt="">
                            </div>
                            <div class="card-body">
                                <h5>Missy Limana<br />
                                    <span> Engineer </span>
                                </h5>
                                <p class="card-text">“ Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil
                                    impedit quo minus id quod maxime placeat ” </p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card text-center">
                            <div class="card-img-top">
                                <img src="img/Martha.jpg" alt="">
                            </div>
                            <div class="card-body">
                                <h5>Martha Brown<br />
                                    <span> Project Manager </span>
                                </h5>
                                <p class="card-text">“ Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil
                                    impedit quo minus id quod maxime placeat ” </p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card text-center">
                           <div class="card-img-top">
                               <img src="img/Alioune.jpg" alt="">
                           </div>
                            <div class="card-body">
                                <h5>Alioune Lisem<br />
                                    <span> Project Manager </span>
                                </h5>
                                <iframe src="img/testimonial-1.mp4" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
    
        </div>
    </div>
    <div class="container">
        <div class="footer-block">
            <button class="btn btn-footer-makabou">
                <img src="img/btn-makabou-footer.png" alt="">
            </button>
            <p class="info-footer-text">Devenez un leader haut niveau grâce à nos conseils d'experts en leadership. Apprenez à inspirer et guider votre équipe vers le succès.
                Rejoignez notre programme de formation dès maintenant et transformez-vous en un leader exceptionnel.</p>
            <p class="copyright-text">© - Copyright, 2023 Makabou by Koncept.Africa,tous droits réservés</p>
        </div>
        <div class="sub-footer">
            <div class="reseau-sociaux-block">
                <a href="">
                    <i class="fa fa-whatsapp"></i>
                </a>
                <a href="">
                    <i class="fa fa-facebook-square"></i>
                </a>
                <a href="">
                    <i class="fa fa-linkedin"></i>
                </a>
                <a href="">
                    <i class="fa fa-instagram"></i>
                </a>
            </div>
            <a href="tel:+221766376425" class="number-phone">+ 221 76 637 64 25</a>
        </div>
    </div>
    
    <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js'></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"  crossorigin="anonymous"></script>
    
    <script>
        $('.video__placeholder, .video__button').on('click', function() {
            if ( !$('#video-player').length ) {
                var video = '<iframe id="video-player" src="' + $('.video__placeholder').attr('data-video') + '" frameborder="0" allowfullscreen wmode="opaque"></iframe>';
                $(video).insertAfter( $('.video__placeholder') );
                $('.video__button').addClass('is-playing');
                $('.video').addClass('go');
            } else {
                $('.video__button').removeClass('is-playing');
                $('#video-player').remove();
                $('.video').removeClass('go');
            }
        });
    </script>
    
    <script>
        $('.video__placeholder2, .video__button2').on('click', function() {
            if ( !$('#video-player2').length ) {
                var video = '<iframe id="video-player2" src="' + $('.video__placeholder2').attr('data-video') + '" frameborder="0" allowfullscreen wmode="opaque"></iframe>';
                $(video).insertAfter( $('.video__placeholder2') );
                $('.video__button2').addClass('is-playing');
                $('.video').addClass('go');
            } else {
                $('.video__button2').removeClass('is-playing');
                $('#video-player2').remove();
                $('.video').removeClass('go');
            }
        });
    </script>
    
    <script>
        $('.video__placeholder3, .video__button3').on('click', function() {
            if ( !$('#video-player3').length ) {
                var video = '<iframe id="video-player3" src="' + $('.video__placeholder3').attr('data-video') + '" frameborder="0" allowfullscreen wmode="opaque"></iframe>';
                $(video).insertAfter( $('.video__placeholder3') );
                $('.video__button3').addClass('is-playing');
                $('.video').addClass('go');
            } else {
                $('.video__button3').removeClass('is-playing');
                $('#video-player3').remove();
                $('.video').removeClass('go');
            }
        });
    </script>
    <script>
    
        $('.multi-item-carousel').on('slide.bs.carousel', function (e) {
            let $e = $(e.relatedTarget),
                itemsPerSlide = 3,
                totalItems = $('.carousel-item', this).length,
                $itemsContainer = $('.carousel-inner', this),
                it = itemsPerSlide - (totalItems - $e.index());
            if (it > 0) {
                for (var i = 0; i < it; i++) {
                    $('.carousel-item', this).eq(e.direction == "left" ? i : 0).
                        // append slides to the end/beginning
                        appendTo($itemsContainer);
                }
            }
        });
    </script>
    
    <script>
        $(".vid-fit-reveal").on('click', function(){
            $(this).addClass( "reveal-video" );
            var myFrame = $(this).find( '#vid-reveal' );
            var url = $(myFrame).attr('src') + '?autoplay=1';
            $(myFrame).attr('src', url);
            $("#js-fitvideo").fitVids();
        });
    
    </script>
    <script id="rendered-js" >
        (function () {
            "use strict";
    
            var carousels = function () {
                $(".owl-carousel1").owlCarousel({
                    loop: true,
                    center: true,
                    margin: 0,
                    responsiveClass: true,
                    nav: false,
                    responsive: {
                        0: {
                            items: 1,
                            nav: false },
    
                        680: {
                            items: 2,
                            nav: false,
                            loop: false },
    
                        1000: {
                            items: 3,
                            nav: true } } });
    
    
    
            };
    
            (function ($) {
                carousels();
            })(jQuery);
        })();
        //# sourceURL=pen.js
    </script>
    <script>
        $('.owl-theme').owlCarousel({
            loop:true,
            margin:10,
            nav: true,
            navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:1,
                    nav:true
                },
                1000:{
                    items:2,
                    nav:true,
                    loop:false
                }
            }
        })
    </script>
    </body>
    </html>